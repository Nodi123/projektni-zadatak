/*
Google map
*/

function initialize() {
  var mapProp = {
    center: new google.maps.LatLng(43.352745, 17.794135),
    zoom: 15,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    scrollwheel: false
  };

  var image = {
    url: 'css/images/iconMap.png',
    size: new google.maps.Size(65, 65)
  };

  var map = new google.maps.Map(document.getElementById("contact-map"), mapProp);

  var marker = new google.maps.Marker({
    map: map,
    position: mapProp.center,
    animation: google.maps.Animation.DROP,
    title: "NSoft, Bleiburških žrtava, Mostar",
    icon: image,
    scrollwheel: mapProp.scrollwheel
  });
}

google.maps.event.addDomListener(window, 'load', initialize);

/******************* 
 * scroll meni ***
 * ****************/


var nav = document.getElementsByClassName("main-menu")[0].children[0];

var navLinks = nav.getElementsByTagName("a");


function removeActiveClass(n, nL) {

  var len = n.childElementCount,

    i;
  console.log(nL);
  for (i = 0; i < len; i++) {
    nL[i].className = "";
  }
}



var navElemsPosition = {
  appLogo: document.getElementById('appLogo').offsetTop - 180,

  aboutus: document.getElementById('aboutus').offsetTop + 900,
  help: document.getElementById("help").offsetTop - 20,
  contactForm: document.getElementById("contactFormMap").offsetTop - 60
};




Object.keys(navLinks).forEach(function (key) {
  var elem = navLinks[key];


  elem.addEventListener('click', function (e) {
    var hash = e.target.hash,
      positon;

    if (!hash) return;

    e.preventDefault();

    removeActiveClass(nav, navLinks); // Remove active class
    this.classList.add('active'); // Add active class

    positon = navElemsPosition[hash.substring(1, hash.length)];

    //kinda smooth scroll 



    var pos = 0;
    var id = setInterval(frame, 1);
    function frame() {

      if (pos >= positon) {
        if (positon == 0)
          window.scrollTo(0, positon);
        clearInterval(id);

      } else {
        pos += 12;

        window.scrollTo(0, pos);
      }

    }





  });
});





/*set scroll active class*/
var scrolPos = window.scrollY,
  timeOut;

function setActiveClass(scrolPos, navElemsPosition, navLinks) {
  if (scrolPos < navElemsPosition.help - 100) {
    navLinks[0].classList.add("active");

  } else if (scrolPos >= navElemsPosition.help - 100 && scrolPos < navElemsPosition.aboutus - 100) {
    navLinks[2].classList.add("active");
  }
  else if (scrolPos >= navElemsPosition.aboutus - 100 && scrolPos < navElemsPosition.contactForm - 100) {
    navLinks[1].classList.add("active");
  } else if (scrolPos >= navElemsPosition.contactForm - 100) {

    navLinks[3].classList.add("active");
  }
}





setActiveClass(scrolPos, navElemsPosition, navLinks);


window.addEventListener('scroll', function (e) {
  clearTimeout(timeOut);

  timeOut = setTimeout(function () {
    scrolPos = window.scrollY;

    removeActiveClass(nav, navLinks);


    setActiveClass(scrolPos, navElemsPosition, navLinks);

  }, 250);
});



var btns = document.getElementsByClassName("switch-btn")[0];
var navBtns = btns.getElementsByTagName("a");
activseter(navBtns, btns);

var btns2 = document.getElementsByClassName("switch-btn2")[0];
var navBtns2 = btns2.getElementsByTagName("a");
activseter(navBtns2, btns2);

var btns3 = document.getElementsByClassName("switch-btn3")[0];
var navBtns3 = btns3.getElementsByTagName("a");
activseter(navBtns3, btns3);

var btns4 = document.getElementsByClassName("switch-btn5")[0];
var navBtns4 = btns4.getElementsByTagName("a");
activseter(navBtns4, btns4);

function activseter(navbl, btnk) {

  Object.keys(navbl).forEach(function (key) {
    var elem = navbl[key];
    elem.addEventListener('click', function (e) {
      var hash = e.target.hash;
      if (!hash) return;
      e.preventDefault();
      removeActiveClass(btnk, navbl); // Remove active class
      this.classList.add('active'); // Add active class

      if (btnk.className == "switch-btn") {
        if (navbl[0].className == "active") {
          navbl[0].parentElement.parentElement.parentElement.children[0].style.display = "block";
          navbl[0].parentElement.parentElement.parentElement.children[1].style.display = "none";
        } else {
          navbl[0].parentElement.parentElement.parentElement.children[1].style.display = "block";
          navbl[0].parentElement.parentElement.parentElement.children[0].style.display = "none";
        }

      } else if (btnk.className == "switch-btn2") {
        if (navbl[0].className == "active") {
          navbl[0].parentElement.parentElement.parentElement.children[2].style.display = "block";
          navbl[0].parentElement.parentElement.parentElement.children[3].style.display = "none";
        } else {
          navbl[0].parentElement.parentElement.parentElement.children[3].style.display = "block";
          navbl[0].parentElement.parentElement.parentElement.children[2].style.display = "none";
        }
      } else if (btnk.className == "switch-btn3") {
        if (navbl[0].className == "active") {
          navbl[0].parentElement.parentElement.parentElement.children[1].style.display = "block";
          navbl[0].parentElement.parentElement.parentElement.children[2].style.display = "none";
          navbl[0].parentElement.parentElement.parentElement.children[3].style.display = "none";
        } else if (navbl[1].className == "active") {
          navbl[0].parentElement.parentElement.parentElement.children[1].style.display = "none";
          navbl[0].parentElement.parentElement.parentElement.children[2].style.display = "block";
          navbl[0].parentElement.parentElement.parentElement.children[3].style.display = "none";
        } else {
          navbl[0].parentElement.parentElement.parentElement.children[1].style.display = "none";
          navbl[0].parentElement.parentElement.parentElement.children[2].style.display = "none";
          navbl[0].parentElement.parentElement.parentElement.children[3].style.display = "block";
        }
      } else if (btnk.className == "switch-btn5") {
        if (navbl[0].className == "active") {
          navbl[0].parentElement.parentElement.parentElement.children[1].style.display = "block";
          navbl[0].parentElement.parentElement.parentElement.children[2].style.display = "none";
          navbl[0].parentElement.parentElement.parentElement.children[3].style.display = "none";
        } else if (navbl[1].className == "active") {
          navbl[0].parentElement.parentElement.parentElement.children[1].style.display = "none";
          navbl[0].parentElement.parentElement.parentElement.children[2].style.display = "block";
          navbl[0].parentElement.parentElement.parentElement.children[3].style.display = "none";
        } else {
          navbl[0].parentElement.parentElement.parentElement.children[1].style.display = "none";
          navbl[0].parentElement.parentElement.parentElement.children[2].style.display = "none";
          navbl[0].parentElement.parentElement.parentElement.children[3].style.display = "block";
        }


      }

    });
  });


}


function sendEmail() 
{
    window.location = "mailto:Info@app.ba";
 
}

/*************************************************
    ***********forma************************
    ******************************************/
 
    function sendData(form){
        var xhttp=new XMLHttpRequest();
        var data="";
           data+="email="+ encodeURIComponent(form.email.value);
        data+="Subject="+ encodeURIComponent(form.Subject.value);
            data+="message="+ encodeURIComponent(form.message.value);
            xhttp.onload=function(e){
                alert(e.target.responseText);
            }
              xhttp.open('POST','#');
            xhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
      xhttp.send(data);
    }
    
    
    var form= document.contactForm;

    form.onsubmit=function(e){
        e.preventDefault();
        
       var reason="";
       reason+=validateSubject(form.Subject);
           reason+=validateEmail(form.email);
        reason+=validateMessage(form.message);
        if(reason>0){
            alert(reason);
            return;
        }
        
        sendData(form);
        form.reset();
    };
    
    
    function validateSubject(Subject){
        
        var error="";
        
        
        if(Subject.value.trim().length==0){
      
      Subject.style.border="1px solid red";
            error="Empty Subject field";
        }else {
            Subject.style.border=0;
            error="";
        }
        return error;
    }
    
        function validateMessage(message){
        
        var error="";
        
      
        if(message.value.trim().length==0){
         
         
       message.style.border="1px solid red";
            error="Empty name field";
        }else {
            message.style.border=0;
            error="";
        }
        return error;
    }
    
    function validateEmail(email){
  
    return  "";
            
        }
    
var loginbtn=document.getElementsByClassName("login-btn")[0].children[0];




loginbtn.addEventListener("click",event);

function event (){

alert("not ready!!! :D")
}
  

